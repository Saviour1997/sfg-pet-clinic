package guru.springframework.sfgpetclinic.bootstrap;
import guru.springframework.sfgpetclinic.model.Owner;
import guru.springframework.sfgpetclinic.model.Pet;
import guru.springframework.sfgpetclinic.model.PetType;
import guru.springframework.sfgpetclinic.model.Vet;
import guru.springframework.sfgpetclinic.services.OwnerService;
import guru.springframework.sfgpetclinic.services.PetTypeService;
import guru.springframework.sfgpetclinic.services.VetService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class DataLoader implements CommandLineRunner {

    private final OwnerService ownerService;
    private final VetService vetService;
    private final PetTypeService petTypeService;

    @Autowired
    public DataLoader(OwnerService ownerService, VetService vetService, PetTypeService petTypeService) {
        this.ownerService = ownerService;
        this.vetService = vetService;
        this.petTypeService = petTypeService;
    }

    @Override
    public void run(String... args) throws Exception {

        PetType dog = new PetType();
        dog.setName("Dog");
        PetType savedDogPetType = petTypeService.save(dog);

        PetType cat = new PetType();
        cat.setName("Cat");
        PetType savedCatPetType = petTypeService.save(cat);

        Owner owner1 = new Owner();
        owner1.setFirstName("Igor");
        owner1.setLastName("Bobrenok");
        owner1.setAddress("Gagarina street");
        owner1.setCity("Ozarichi");
        owner1.setTelephone("12312312312");

        Pet pet1 = new Pet();
        pet1.setPetType(savedDogPetType);
        pet1.setOwner(owner1);
        pet1.setBirthDate(LocalDate.now());
        pet1.setName("Baikal");
        owner1.getPets().add(pet1);

        ownerService.save(owner1);

        Owner owner2 = new Owner();
        owner2.setFirstName("Igor");
        owner2.setLastName("Bobrenok2");
        owner2.setAddress("Gagarina street");
        owner2.setCity("Ozarichi");
        owner2.setTelephone("12312312312");
        ownerService.save(owner2);

        Pet pet2 = new Pet();
        pet2.setPetType(savedCatPetType);
        pet2.setOwner(owner2);
        pet2.setBirthDate(LocalDate.now());
        pet2.setName("Musya");
        owner2.getPets().add(pet2);

        System.out.println("Loaded owners");

        Vet vet1 = new Vet();
        vet1.setFirstName("sadasd");
        vet1.setLastName("dsafsdf");
        vetService.save(vet1);

        Vet vet2 = new Vet();
        vet2.setFirstName("dfsdf");
        vet2.setLastName("dsafssdfdf");
        vetService.save(vet2);
        System.out.println("Loaded vets");

    }
}
